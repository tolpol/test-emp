<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DepartmentRepository")
 * @ORM\Table(name="departments")
 */
class Department
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", name="dept_no")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="dept_name")
     */
    private $name;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
