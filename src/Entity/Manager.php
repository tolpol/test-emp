<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ManagerRepository")
 * @ORM\Table(name="dept_manager")
 */
class Manager
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="App\Entity\Employee", fetch="EAGER")
     * @ORM\JoinColumn(name="emp_no", referencedColumnName="emp_no")
     * @ORM\OrderBy({"hireDate" = "ASC"})
     */
    private $employee;

    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="App\Entity\Department")
     * @ORM\JoinColumn(name="dept_no", referencedColumnName="dept_no")
     */
    protected $departmentId;

    /**
     * @ORM\Column(type="date")
     */
    protected $fromDate;

    /**
     * @ORM\Column(type="date")
     */
    protected $toDate;

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return mixed
     */
    public function getDepartmentId()
    {
        return $this->departmentId;
    }

    /**
     * @return mixed
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * @return mixed
     */
    public function getToDate()
    {
        return $this->toDate;
    }
}
