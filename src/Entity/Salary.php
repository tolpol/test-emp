<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SalaryRepository")
 * @ORM\Table(name="salaries")
 */
class Salary
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="emp_no")
     */
    private $employeeId;

    /**
     * @ORM\Column(type="integer", name="salary")
     */
    private $salary;

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $fromDate;

    /**
     * @ORM\Column(type="date")
     */
    private $toDate;

    /**
     * @return mixed
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @return mixed
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * @return mixed
     */
    public function getToDate()
    {
        return $this->toDate;
    }
}
