<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 * @ORM\Table(name="employees")
 */
class Employee
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="emp_no")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @ORM\Column(type="enumemployeegender")
     */
    private $gender;

    /**
     * @ORM\Column(type="date")
     */
    private $hireDate;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Department", fetch="EAGER", inversedBy="employees")
     * @ORM\JoinTable(name="dept_emp",
     *      joinColumns={@ORM\JoinColumn(name="emp_no", referencedColumnName="emp_no")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="dept_no", referencedColumnName="dept_no")}
     *      )
     */
    private $departments;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @return mixed
     */
    public function getHireDate()
    {
        return $this->hireDate;
    }

    /**
     * @return mixed
     */
    public function getDepartments()
    {
        return $this->departments;
    }
}
