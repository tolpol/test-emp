<?php

namespace App\Controller;

use App\Repository\DepartmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @var DepartmentRepository
     */
    private $departmentRepository;

    /**
     * DefaultController constructor.
     * @param DepartmentRepository $departmentRepository
     */
    public function __construct(DepartmentRepository $departmentRepository)
    {
        $this->departmentRepository = $departmentRepository;
    }

    /**
     * @Route("/", name="main")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
     public function index(Request $request)
     {
         $departments = $this->departmentRepository->findAll();

         return $this->render('index.html.twig', [
             "departments" => $departments
         ]);
     }
}