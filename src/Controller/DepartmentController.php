<?php

namespace App\Controller;

use App\Repository\DepartmentRepository;
use App\Repository\EmployeeRepository;
use App\Repository\ManagerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/department")
 */
class DepartmentController extends AbstractController
{
    /**
     * @var DepartmentRepository
     */
    private $departmentRepository;

    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * @var ManagerRepository
     */
    private $managerRepository;

    /**
     * DefaultController constructor.
     * @param DepartmentRepository $departmentRepository
     * @param EmployeeRepository $employeeRepository
     * @param ManagerRepository $managerRepository
     */
    public function __construct(
        DepartmentRepository $departmentRepository,
        EmployeeRepository $employeeRepository,
        ManagerRepository $managerRepository
    )
    {
        $this->departmentRepository = $departmentRepository;
        $this->employeeRepository = $employeeRepository;
        $this->managerRepository = $managerRepository;
    }

    /**
     * @Route("/{id}/show", name="department_show", methods="GET|POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
     public function show(Request $request)
     {
         $id = $request->get("id");

         $managers = $this->managerRepository->findBy(["departmentId"=> $id]);
         $employees = $this->employeeRepository->findByDepartmentId($id);

         return $this->render('department/show.html.twig', [
             "managers" => $managers,
             "employees" => $employees
         ]);
     }
}