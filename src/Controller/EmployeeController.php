<?php

namespace App\Controller;

use App\Repository\DepartmentRepository;
use App\Repository\EmployeeRepository;
use App\Repository\ManagerRepository;
use App\Repository\SalaryRepository;
use App\Repository\TitleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/employee")
 */
class EmployeeController extends AbstractController
{
    /**
     * @var DepartmentRepository
     */
    private $departmentRepository;

    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * @var ManagerRepository
     */
    private $managerRepository;

    /**
     * @var SalaryRepository
     */
    private $salaryRepository;

    /**
     * @var TitleRepository
     */
    private $titleRepository;

    /**
     * DefaultController constructor.
     * @param DepartmentRepository $departmentRepository
     * @param EmployeeRepository $employeeRepository
     * @param ManagerRepository $managerRepository
     * @param SalaryRepository $salaryRepository
     * @param TitleRepository $titleRepository
     */
    public function __construct(
        DepartmentRepository $departmentRepository,
        EmployeeRepository $employeeRepository,
        ManagerRepository $managerRepository,
        SalaryRepository $salaryRepository,
        TitleRepository $titleRepository
    )
    {
        $this->departmentRepository = $departmentRepository;
        $this->employeeRepository = $employeeRepository;
        $this->managerRepository = $managerRepository;
        $this->salaryRepository = $salaryRepository;
        $this->titleRepository = $titleRepository;
    }

    /**
     * @Route("/{id}/show", name="employee_show", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
     public function show(Request $request)
     {
         $id = $request->get("id");
         $manager = $this->managerRepository->findOneBy(["employee" => $id]);
         $employee = $this->employeeRepository->find($id);
         $departments = $this->departmentRepository->findByEmployeeId($id);
         $salaries = $this->salaryRepository->findBy(["employeeId" => $id], ["fromDate" => "ASC"]);
         $positions = $this->titleRepository->findBy(["employeeId" => $id], ["fromDate" => "ASC"]);

         return $this->render('employee/show.html.twig', [
             "employee" => $employee,
             "salaries" => $salaries,
             "positions" => $positions,
             "departments" => $departments,
             "manager" => $manager
         ]);
     }
}