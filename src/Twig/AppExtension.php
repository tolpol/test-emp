<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('create_period', [$this, 'createPeriodIfValid']),
        ];
    }

    public function createPeriodIfValid($fromDate, $toDate, $format)
    {
        if(is_string($fromDate)) {
           $from = date($format, strtotime($fromDate));
        } else {
           $from = $fromDate->format($format);
        }

        if(is_string($toDate)) {
            $to = $toDate;
        } else {
            $to = $toDate->format($format);
        }

        if (strpos($to, '9999') !== false) {
            return $from;
        }

        return sprintf("%s - %s", $from, date($format, strtotime($to)));
    }
}