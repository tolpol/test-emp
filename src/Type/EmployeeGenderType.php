<?php

namespace App\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class EmployeeGenderType extends Type
{
    const GENDER_MALE = "M";
    const GENDER_FEMALE = "F";

    private static $values = [
        self::GENDER_MALE,
        self::GENDER_FEMALE,
    ];

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        $values = array_map(function (string $value) {
            return "'$value'";
        }, self::$values);

        return "ENUM(" . join(", ", $values) . ")";
    }

    public function getName()
    {
        return "enumemployeegender";
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}