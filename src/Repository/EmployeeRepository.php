<?php

namespace App\Repository;

use App\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Employee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Employee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Employee[]    findAll()
 * @method Employee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Employee::class);
    }

    /**
     * @param string $departmentId
     * @return Employee[]
     */
    public function findByDepartmentId(string $departmentId)
    {
        $qb = $this->createQueryBuilder("e");
        $query = $qb
            ->innerJoin("e.departments", "d")
            ->where("d.id = :id")
            ->setParameter("id", $departmentId)
            ->addOrderBy("e.id", "DESC")
            ->getQuery();

        return $query->getResult();
    }
}
