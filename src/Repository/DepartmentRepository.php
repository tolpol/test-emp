<?php

namespace App\Repository;

use App\Entity\Department;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Department|null find($id, $lockMode = null, $lockVersion = null)
 * @method Department|null findOneBy(array $criteria, array $orderBy = null)
 * @method Department[]    findAll()
 * @method Department[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DepartmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Department::class);
    }

    public function findByEmployeeId(string $employeeId)
    {
        $conn = $this->getEntityManager()->getConnection();

        $stmt = $conn->prepare(
            "SELECT de.from_date AS fromDate, de.to_date AS toDate, d.dept_name AS 'name'
                   FROM departments AS d 
              LEFT JOIN dept_emp AS de
                     ON d.dept_no = de.dept_no
                  WHERE de.emp_no = :id
                  ORDER BY de.from_date ASC"
        );

        $stmt->execute(['id' => $employeeId]);
        return $stmt->fetchAllAssociative();
    }
}
