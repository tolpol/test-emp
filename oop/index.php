<?php
$res = glob("*.html");
foreach ($res as $item) {
    echo $item . PHP_EOL;
}

$fp = fopen("foo.txt", "w");
fwrite($fp, "uiuiuiuiui");
//fclose($fp);

echo get_resource_type($fp) . "\n";

switch (8.08) {
    case '8.0':
        $result = "Oh no!";
        break;
    case 8.08:
        $result = "This is what I expected";
        break;
}
echo $result;
 echo 0 == 'foobar';

function test()
{
    static $a = 0;
    echo $a . PHP_EOL;
    $a++;
    usleep(1);
    test();
}

//test();

$a = 1;
$b = 2;

function Sum()
{
    global $a, $b;

    $b = $a + $b;
}

Sum();
echo PHP_EOL . $b;


$a = 1;
$b = 2;

function Sumc()
{
    $GLOBALS['b'] = $GLOBALS['a'] + $GLOBALS['b'];
}

Sumc();
echo PHP_EOL . $b;
echo PHP_EOL;
//var_dump($GLOBALS["_SERVER"]);
$r = 'var_dump($GLOBALS["_SERVER"])';
//eval('var_dump($GLOBALS["_SERVER"]);');


class MethodTest {
    public function __call($name, $arguments) {
        // Замечание: значение $name регистрозависимо.
        echo "Вызов метода '$name' "
            . implode(', ', $arguments). "\n";
    }

    /**  Начиная с PHP 5.3.0  */
    public static function __callStatic($name, $arguments) {
        // Замечание: значение $name регистрозависимо.
        echo "Вызов статического метода '$name' "
            . implode(', ', $arguments). "\n";
    }
}

$obj = new MethodTest;
$obj->runTest('в контексте объекта', "ffff");

MethodTest::runTest('в статическом контексте');

$a = new stdClass();
$a->tt = 77;
echo $a->tt;

class Nr {
    public $r;

    protected static $stat;
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
        echo "visivaetsya";
        self::$stat = 55;
    }

    public function __call($name, $arguments)
    {
        echo 3;
    }

    public function __toString()
    {
        return "fffffff";
        ///$this->__call($w, $r);
    }
}

class Mk extends Nr{
    public function __construct($name)
    {
        parent::__construct($name);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStat()
    {
        return self::$stat;
    }

    public function __get($name)
    {
        echo "gggggggggggggg";
    }

    public function __set($name, $val)
    {
        echo "tttttset";
    }
}

$m = new Mk("igor");
//echo $m->name = "vasya";
//echo $m->name;
echo $m->ddd;
//echo $m->getName();
//echo $m->getStat();

//$a = new Nr("Igor");
//echo $a;
//$a->call();


function test5()
{
    static $count = 0;
static $count = 5;
    $count++;
    echo $count;
    if ($count < 10) {
        test5();
    }
    $count--;

}

$a = ["n" => 34];
$b = ["m" => 545];

$c = $a + $b;
var_dump($c);

abstract class Ab
{
    abstract function read();
}

class NonAb extends Ab {
    public function read()
    {
        echo 2;
    }
}

$ab = new NonAb();
$ab->read();
